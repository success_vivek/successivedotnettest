﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ScheduledInterviewDetails.aspx.cs" Inherits="SuccessiveDotNetTest.ScheduledInterviewDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <br />
    </div>
    <div class="form-group row">
        <div style="float:right">
            <asp:Button ID="btnSScheduleInterview" runat="server" CssClass="btn btn-primary" Text="Schedule Interview" OnClick="btnSScheduleInterview_Click" />
        </div>
    </div>
    <asp:GridView runat="server" ID="gvRecords" PageSize="25" AllowPaging="true" AutoGenerateColumns="false" 
        OnRowDataBound="gvRecords_RowDataBound" OnPageIndexChanging="gvRecords_PageIndexChanging" CssClass="table" AllowSorting="true" OnSorting="gvRecords_Sorting">
        <Columns>
                <asp:BoundField DataField="" HeaderText="Name" SortExpression="Asc"/>
                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                <asp:BoundField DataField="" HeaderText="Experience" SortExpression="Experience" />
                <asp:BoundField DataField="" HeaderText="Schedule Date and Time" />
            </Columns>
    </asp:GridView>
</asp:Content>
