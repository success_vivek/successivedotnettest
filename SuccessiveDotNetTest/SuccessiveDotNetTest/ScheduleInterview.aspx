﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ScheduleInterview.aspx.cs" Inherits="SuccessiveDotNetTest.ScheduleInterview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3 runat="server" id="heading">Schedule Interview Form</h3>
    <hr />


    <div class="form-group row">
        <label for="lblCandidateName" class="col-sm-3 col-form-label"><font color="red">*</font>Candidate Name</label>
        <div class="col-sm-4">
            <asp:DropDownList ID="ddlCandidateName" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlCandidateName_SelectedIndexChanged" AutoPostBack="true">
            </asp:DropDownList>
        </div>
        <asp:CheckBox ID="cbNewCandidate" runat="server" Text="New Candidate" AutoPostBack="true" OnCheckedChanged="cbNewCandidate_CheckedChanged"></asp:CheckBox>
    </div>


    <div class="form-group row">
        <label for="lblFirstName" class="col-sm-3 col-form-label"><font color="red">*</font>First Name</label>
        <div class="col-sm-9">
            <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ErrorMessage="First Name Field is Required"
                ControlToValidate="txtFirstName" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revFirstName" ControlToValidate="txtFirstName" runat="server"
                ValidationExpression="^[a-zA-Z]{3,50}" Display="Dynamic" ErrorMessage="Enter valid Name with less than 50 character"
                ForeColor="Red"></asp:RegularExpressionValidator>
        </div>
    </div>

    <div class="form-group row">
        <label for="lblLastName" class="col-sm-3 col-form-label"><font color="red">*</font>Last Name</label>
        <div class="col-sm-9">
            <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ErrorMessage="Last Name Field is Required"
                ControlToValidate="txtLastName" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revLastName" ControlToValidate="txtLastName"
                runat="server" ValidationExpression="^[a-zA-Z'.\s]{3,50}" Display="Dynamic" ErrorMessage="Name is not valid"
                ForeColor="Red"></asp:RegularExpressionValidator>
        </div>
    </div>

    <div class="form-group row">
        <label for="lblEmail" class="col-sm-3 col-form-label"><font color="red">*</font>Email</label>
        <div>
            <div class="col-sm-9">
                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Email Field is Required" Display="Dynamic"
                    ControlToValidate="txtEmail" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic"
                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red"
                    ErrorMessage="Enter a valid email"></asp:RegularExpressionValidator>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="lblDOB" class="col-sm-3 col-form-label"><font color="red">*</font>Date Of Birth</label>
        <div class="col-sm-9">
            <asp:TextBox ID="txtDob" runat="server" CssClass="form-control" TextMode="Date" />
            <asp:RequiredFieldValidator ID="rfvDob" runat="server" ErrorMessage="Dob Field is Required" Display="Dynamic"
                ForeColor="Red" ControlToValidate="txtDob"></asp:RequiredFieldValidator>
        </div>
    </div>

    <div class="form-group row">
        <label for="lblMobile" class="col-sm-3 col-form-label">&nbsp;Mobile</label>
        <div class="col-sm-9">
            <asp:TextBox ID="txtMobile" runat="server" placeholder="(22)222 2222" CssClass="form-control" TextMode="Phone" MaxLength="10"></asp:TextBox>
            <asp:RegularExpressionValidator ID="revMobile" runat="server" ControlToValidate="txtMobile" ValidationExpression="[0-9]{10}"
                Display="Dynamic" ForeColor="Red" ErrorMessage="Mobile no. must contain 10 numeric digits"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="rvMobile" runat="server" ControlToValidate="txtMobile" MinimumValue="6000000000" MaximumValue="9999999999"
                Display="Dynamic" Type="Double" ForeColor="Red" ErrorMessage="Mobile no. must start with no. greater than 5"></asp:RangeValidator>
        </div>
    </div>

    <div class="form-group row">
        <label for="lblExperience" class="col-sm-3 col-form-label"><font color="red">*</font>Experience (Months)</label>
        <div class="col-sm-9">
            <asp:TextBox ID="txtExperience" CssClass="form-control" runat="server" Display="Dynamic" MaxLength="4"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvExperience" runat="server" ErrorMessage="Experince Field is Required" Display="Dynamic" ControlToValidate="txtExperience"
                ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revExperience" runat="server" ControlToValidate="txtExperience" ValidationExpression="[0-9]{0,4}" Display="Dynamic"
                ForeColor="Red" ErrorMessage="Enter valid month of experience"></asp:RegularExpressionValidator>
        </div>
    </div>

    <div class="form-group row">
        <label for="lblDate" class="col-sm-3 col-form-label"><font color="red">*</font>Date</label>
        <div class="col-sm-9">
            <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" TextMode="Date" />
            <asp:RequiredFieldValidator ID="rfvDate" runat="server" ErrorMessage="Scheduled date Field is Required" Display="Dynamic" ForeColor="Red"
                ControlToValidate="txtDate"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rvDate" ControlToValidate="txtDate" runat="server" Display="Dynamic" ErrorMessage="Selected date must not be a past date"
                 ForeColor="Red"></asp:RangeValidator>
            <%--<asp:CompareValidator ValueToCompare="<%= DateTime.Now.ToShortDateString (); %>" ID="cvData" ControlToValidate="txtDate" 
                ErrorMessage="Selected date must not be a past date" runat="server" Type="Date" Operator="GreaterThan"></asp:CompareValidator>--%>

        </div>
    </div>

    <div class="form-group row">
        <label for="lblTimeFrom" class="col-sm-3 col-form-label"><font color="red">*</font>Time From</label>
        <div class="col-sm-9">
            <asp:TextBox ID="txtTimeFrom" runat="server" CssClass="form-control" TextMode="Time" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Time From is Required" Display="Dynamic" ForeColor="Red"
                ControlToValidate="txtTimeFrom"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rvTimeFrom" ControlToValidate="txtTimeFrom" runat="server" MinimumValue="08:00" MaximumValue="22:00" Display="Dynamic"
                ErrorMessage="Time must be between 8:00 AM to 10:00 PM" ForeColor="Red"></asp:RangeValidator>
         </div>
    </div>

    <div class="form-group row">
        <label for="lblTimeTo" class="col-sm-3 col-form-label"><font color="red">*</font>Time To</label>
        <div class="col-sm-9">
            <asp:TextBox ID="txtTimeTo" runat="server" CssClass="form-control" TextMode="Time" />
            <asp:RangeValidator ID="rfTimeTo" ControlToValidate="txtTimeTo" runat="server" MinimumValue="08:00" MaximumValue="22:00" Display="Dynamic"
                ErrorMessage="Time must be between 8:00 AM to 10:00 PM" ForeColor="Red"></asp:RangeValidator>
            <asp:CompareValidator ID="cvTimeTo" ControlToValidate="txtTimeTo" ControlToCompare="txtTimeFrom" Operator="GreaterThan" runat="server"
                ErrorMessage="Selected time must be greater than Time From" ForeColor="Red"></asp:CompareValidator>
        </div>
    </div>

    <div class="form-group row">
        <label for="lblInterviewer" class="col-sm-3 col-form-label">&nbsp;Interviewer</label>
        <div class="col-sm-9">
            <asp:TextBox ID="txtInterviewer" runat="server" CssClass="form-control" AutoCompleteType="Disabled"></asp:TextBox>
            <asp:RegularExpressionValidator ID="revInterviewer" ControlToValidate="txtInterviewer" runat="server" ValidationExpression="^[a-zA-Z'.\s]{3,50}"
                Display="Dynamic" ErrorMessage="Name is not valid" ForeColor="Red"></asp:RegularExpressionValidator>
        </div>
    </div>

    <div class="form-group row">
        <div style="margin-left: 460px">
            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="btnSubmit_Click" />
            <asp:Button ID="btnCancel" runat="server" CssClass="btn btn" Text="Cancel" CausesValidation="False" OnClick="btnCancel_Click" />
        </div>
    </div>

</asp:Content>
