﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace BusinessLogic
{
    public class BusinessLogicLayer
    {
        DataAccessLayer dataAccessLayer = new DataAccessLayer();
        public DataTable GetCandidates()
        {

            List<Candidate> candidates = dataAccessLayer.GetCandidates();
            List<CandidateRecord> candidateRecords = new List<CandidateRecord>();

            candidateRecords = candidates.Select(s => new CandidateRecord
            {
                Id = s.Id,
                FirstName = s.FirstName,
                LastName = s.LastName,
                DOB = s.DOB,
                Email = s.Email,
                Experience = s.Experience,
                Mobile = s.Mobile
            }).ToList();

            DataTable table = new DataTable();
            DataColumn column;
            DataRow row;

            column = new DataColumn
            {
                DataType = System.Type.GetType("System.String"),
                ColumnName = "Id"
            };
            table.Columns.Add(column);

            column = new DataColumn
            {
                DataType = System.Type.GetType("System.String"),
                ColumnName = "Details"
            };
            table.Columns.Add(column);

            foreach (var array in candidateRecords)
            {
                row = table.NewRow();
                row["Id"] = array.Id;
                row["Details"] = array.FirstName + " " + array.LastName + " (" + array.Email + " )";
                table.Rows.Add(row);
            }
            return table;
        }

        public CandidateRecord GetCandidateData(string id)
        {
            Candidate candidateDetails = dataAccessLayer.GetCandidateRecord(id);
            CandidateRecord fetchCandidateDetails = new CandidateRecord()
            {
                Id = candidateDetails.Id,
                FirstName = candidateDetails.FirstName,
                LastName = candidateDetails.LastName,
                Experience = candidateDetails.Experience,
                Email = candidateDetails.Email,
                DOB = candidateDetails.DOB,
                Mobile = candidateDetails.Mobile
            };
            return fetchCandidateDetails;
        }

        public void RegisterNewCandidate(CandidateRecord candidateDetails, Interview interview)
        {
            Candidate candidateObj = new Candidate()
            {
                FirstName = candidateDetails.FirstName,
                LastName = candidateDetails.LastName,
                Email = candidateDetails.Email,
                DOB = candidateDetails.DOB,
                Experience = candidateDetails.Experience,
                Mobile = candidateDetails.Mobile
            };
            InterviewSchedule interviewObj = new InterviewSchedule()
            {
                Date = interview.Date,
                StartDateTime = interview.StartDateTime,
                EndDateTime = interview.EndDateTime,
                InterviewerName = interview.InterviewerName,
            };
            dataAccessLayer.RegisterNewCandidate(candidateObj, interviewObj);
        }
        public bool CheckNewRegistrationEmail(string email)
        {
            return dataAccessLayer.CheckRegistrationEmail(email);
        }


        public void UpdateCandidate(CandidateRecord userDetails, Interview interview)
        {
            Candidate UpdateUserDetails = new Candidate()
            {
                Id = userDetails.Id,
                Experience = userDetails.Experience,
                Mobile = userDetails.Mobile
            };
            InterviewSchedule interviewSchedule = new InterviewSchedule()
            {
                Date = interview.Date,
                StartDateTime = interview.StartDateTime,
                EndDateTime = interview.EndDateTime,
                InterviewerName = interview.InterviewerName
            };
            dataAccessLayer.UpdateCandidateDetails(UpdateUserDetails, interviewSchedule);
        }

        public List<CandidateRecord> GetRecords()
        {
            List<Candidate> dtUserDetails = dataAccessLayer.GetRecords();
            List<CandidateRecord> candidateDetails = new List<CandidateRecord>();
            candidateDetails = dtUserDetails.Select(s => new CandidateRecord
            {
                FirstName=s.FirstName,
                LastName=s.LastName,
                Experience=s.Experience,
                Email = s.Email
            }).ToList();
            return candidateDetails;
        }
    }
}
