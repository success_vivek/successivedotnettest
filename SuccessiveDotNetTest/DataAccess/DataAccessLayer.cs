﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class DataAccessLayer
    {
        public List<Candidate> GetCandidates()
        {
            return new DatabaseEntities().Candidates.ToList();
        }

        public Candidate GetCandidateRecord(string id)
        {
            int candidateID = Convert.ToInt32(id);
            var candidateDetails = new DatabaseEntities().Candidates.Where(x => x.Id.Equals(candidateID)).First();
            Candidate storeCandidateDetails = new Candidate()
            {

                Id = candidateDetails.Id,
                FirstName = candidateDetails.FirstName,
                LastName = candidateDetails.LastName,
                Mobile = candidateDetails.Mobile,
                DOB = candidateDetails.DOB,
                Email = candidateDetails.Email,
                Experience = candidateDetails.Experience
            };
            return storeCandidateDetails;
        }

        public void RegisterNewCandidate(Candidate candidateDetails, InterviewSchedule interview)
        {
            var newCandidate = new Candidate()
            {
                FirstName = candidateDetails.FirstName,
                LastName = candidateDetails.LastName,
                Email = candidateDetails.Email,
                DOB = candidateDetails.DOB,
                Experience = candidateDetails.Experience,
                Mobile = candidateDetails.Mobile
            };

            var context = new DatabaseEntities();

            context.Candidates.Add(newCandidate);
            context.SaveChanges();

            var userDetails = new DatabaseEntities().Candidates.Max(p => p.Id);
            Candidate candidateId = new Candidate()
            {
                Id = userDetails
            };
            var interviewDetails = new InterviewSchedule()
            {
                CandidateId = candidateId.Id,
                Date = interview.Date,
                StartDateTime = interview.StartDateTime,
                EndDateTime = interview.EndDateTime,
                InterviewerName = interview.InterviewerName,
            };

            context.InterviewSchedules.Add(interviewDetails);
            context.SaveChanges();
        }
        public bool CheckRegistrationEmail(string email)
        {
            return new DatabaseEntities().Candidates.Count(x => x.Email.Equals(email)) == 1;
        }

        public void UpdateCandidateDetails(Candidate updateCandiate, InterviewSchedule interview)
        {
            var context = new DatabaseEntities();
            int id = Convert.ToInt16(updateCandiate.Id);
            var userDetail = context.Candidates.FirstOrDefault(x => x.Id == id);
            if (userDetail != null)
            {
                userDetail.Experience = updateCandiate.Experience;
                userDetail.Mobile = updateCandiate.Mobile;
            }
            context.SaveChanges();

            var interviewDetails = new InterviewSchedule()
            {
                CandidateId = id,
                Date = interview.Date,
                StartDateTime = interview.StartDateTime,
                EndDateTime = interview.EndDateTime,
                InterviewerName = interview.InterviewerName,
            };
            context.InterviewSchedules.Add(interviewDetails);
            context.SaveChanges();
        }
        public List<Candidate> GetRecords()
        {
            return new DatabaseEntities().Candidates.ToList();
        }
    }
}
