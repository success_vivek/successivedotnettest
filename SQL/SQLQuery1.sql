use test

Create table Candidates
(
     Id int primary key identity,
     FirstName varchar(50) NOT NULL,
	 LastName varchar(50)  NOT NULL,
     DOB date  NOT NULL,
	 Experience int  NOT NULL,
	 Mobile varchar(10),
	 Email varchar(50) NOT NULL UNIQUE,
)

select * from Candidates
