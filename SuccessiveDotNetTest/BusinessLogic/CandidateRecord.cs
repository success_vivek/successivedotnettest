﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class CandidateRecord
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public System.DateTime DOB { get; set; }
        public int Experience { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        //public static implicit operator CandidateRecord(CandidateRecord v)
        //{
        //    throw new NotImplementedException();
        //}
    }

    public class Interview
    {
        public int Id { get; set; }
        public Nullable<int> CandidateId { get; set; }
        public System.DateTime Date { get; set; }
        public System.DateTime StartDateTime { get; set; }
        public Nullable<System.DateTime> EndDateTime { get; set; }
        public string InterviewerName { get; set; }
    }
}
