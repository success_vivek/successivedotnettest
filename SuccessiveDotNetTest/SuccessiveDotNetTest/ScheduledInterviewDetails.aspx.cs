﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;

namespace SuccessiveDotNetTest
{
    public partial class ScheduledInterviewDetails : System.Web.UI.Page
    {
        BusinessLogicLayer businessLogicLayer = new BusinessLogicLayer();
        protected void Page_Load(object sender, EventArgs e)
        {
            BindGridView();
        }
        protected void BindGridView()
        {
            gvRecords.DataSource = businessLogicLayer.GetRecords();
            gvRecords.DataBind();
        }

        protected void gvRecords_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[0].Text = string.Format("{0}&nbsp;{1}", DataBinder.Eval(e.Row.DataItem, "FirstName"), DataBinder.Eval(e.Row.DataItem, "LastName"));
                
                var year = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Experience"))/12;
                var months = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Experience")) % 12;

                if (year > 0)
                {
                    string experience = year + "years" + "&nbsp;" + months + "months";
                    e.Row.Cells[2].Text = experience;
                }
                else
                {
                    string experience = months + "months";
                    e.Row.Cells[2].Text = experience;
                }
                
            }
        }

        protected void gvRecords_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvRecords.PageIndex = e.NewPageIndex;
            gvRecords.DataBind();
        }

        protected void btnSScheduleInterview_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ScheduleInterview.aspx");
        }

        protected void gvRecords_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

    }


}