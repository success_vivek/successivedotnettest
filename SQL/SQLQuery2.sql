use test

Create table InterviewSchedules
(
     Id int primary key identity(1,1),
	 CandidateId int FOREIGN KEY REFERENCES Candidates(Id),
	 Date date  NOT NULL,
	 StartDateTime datetime not null,
	 EndDateTime datetime,
	 InterviewerName varchar(100)
)

select * from InterviewSchedules