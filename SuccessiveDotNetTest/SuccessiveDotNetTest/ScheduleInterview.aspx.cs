﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;

namespace SuccessiveDotNetTest
{
    public partial class ScheduleInterview : System.Web.UI.Page
    {
        BusinessLogicLayer businessLogicLayer = new BusinessLogicLayer();
        protected void Page_Load(object sender, EventArgs e)
        {
            rvDate.MaximumValue = DateTime.Today.AddYears(2).ToShortDateString();
            rvDate.MinimumValue = DateTime.Today.ToShortDateString();
            if (!IsPostBack)
            {
                if (Request.QueryString["Id"] != null)
                {
                    string id = Request.QueryString["Id"].ToString();
                    CandidateRecord candidateRecord = businessLogicLayer.GetCandidateData(id);
                    txtFirstName.Text = candidateRecord.FirstName;
                    txtLastName.Text = candidateRecord.LastName;
                    txtDob.Text = Convert.ToDateTime(candidateRecord.DOB).ToShortDateString();
                    txtDob.Enabled = false;
                    txtEmail.Text = candidateRecord.Email;
                    txtFirstName.Enabled = false;
                    txtLastName.Enabled = false;
                    txtEmail.Enabled = false;
                    txtMobile.Text = candidateRecord.Mobile;
                    txtExperience.Text = candidateRecord.Experience.ToString();
                }
                BindCandidateRecords();
            }
        }

        protected void BindCandidateRecords()
        {
            DataTable dt = businessLogicLayer.GetCandidates();
            ddlCandidateName.DataSource = dt;
            ddlCandidateName.DataTextField = dt.Columns[1].ToString();
            ddlCandidateName.DataValueField = dt.Columns[0].ToString();
            ddlCandidateName.DataBind();
            ddlCandidateName.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        protected void ddlCandidateName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id = ddlCandidateName.SelectedValue;
            Response.Redirect("~/ScheduleInterview.aspx?Id=" + id);
        }

        protected void cbNewCandidate_CheckedChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (cbNewCandidate.Checked == true)
                {
                    ddlCandidateName.SelectedValue = "0";
                    ddlCandidateName.Enabled = false;
                    txtFirstName.Enabled = true;
                    txtLastName.Enabled = true;
                    txtDob.Enabled = true;
                    txtEmail.Enabled = true;
                    txtFirstName.Text = "";
                    txtLastName.Text = "";
                    txtDob.Text = "";
                    txtEmail.Text = "";
                    txtMobile.Text = "";
                    txtExperience.Text = "";

                }
                else
                {
                    ddlCandidateName.Enabled = true;
                }
            }

        }

        public void AddCandidateRecord()
        {

            bool result = businessLogicLayer.CheckNewRegistrationEmail(txtEmail.Text);
            if (result == true)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "msg", "alert('This email already exist! Please enter a different email id')", true);
            }
            else
            {
                CandidateRecord candidate = new CandidateRecord()
                {
                    FirstName = txtFirstName.Text,
                    LastName = txtLastName.Text,
                    DOB = Convert.ToDateTime(txtDob.Text),
                    Email = txtEmail.Text,
                    Mobile = txtMobile.Text,
                    Experience = Convert.ToInt32(txtExperience.Text)
                };
                Interview interview = new Interview()
                {
                    Date = Convert.ToDateTime(txtDate.Text),
                    StartDateTime = Convert.ToDateTime(txtTimeFrom.Text),
                    EndDateTime = Convert.ToDateTime(txtTimeTo.Text),
                    InterviewerName = txtInterviewer.Text
                };
                businessLogicLayer.RegisterNewCandidate(candidate, interview);
                Response.Redirect("~/ScheduleInterview.aspx");
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                if (Request.QueryString["Id"] == null)
                {
                    AddCandidateRecord();
                }


                else if (Request.QueryString["Id"] != null && cbNewCandidate.Checked == true)
                {
                    AddCandidateRecord();
                }

                else
                {
                    int userId = Convert.ToInt32(Request.QueryString["Id"]);
                    CandidateRecord candidateRecord = new CandidateRecord()
                    {
                        Id = userId,
                        Mobile = txtMobile.Text,
                        Experience = Convert.ToInt32(txtExperience.Text)
                    };
                    Interview interview = new Interview()
                    {
                        Date = Convert.ToDateTime(txtDate.Text),
                        StartDateTime = Convert.ToDateTime(txtTimeFrom.Text),
                        EndDateTime = Convert.ToDateTime(txtTimeTo.Text),
                        InterviewerName = txtInterviewer.Text
                    };
                    businessLogicLayer.UpdateCandidate(candidateRecord, interview);
                    Response.Redirect("~/ScheduleInterview.aspx");
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ScheduleInterview.aspx");
        }
    }
}